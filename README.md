# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Internal repository for artifacts
* version: 1.0

### How do I get set up? ###

# For Gradle Use case #
* For deploying artifacts use 'uploadArchives' task e.g. gradble build uploadArchives
* repo urls:
  * snapshot repo: https://api.bitbucket.org/1.0/repositories/Anjan0791/maven-internal/raw/snapshots
  * releases repo: https://api.bitbucket.org/1.0/repositories/Anjan0791/maven-internal/raw/releases
  * use below snippet in build.gradle

```
allprojects {
     repositories {
        mavenCentral()
         maven {
             url "https://api.bitbucket.org/1.0/repositories/Anjan0791/maven-internal/raw/snapshots"
         }
         maven {
             url "https://api.bitbucket.org/1.0/repositories/Anjan0791/maven-   internal/raw/releases"
        }
     }
 }
```

* It is recommended to run gradle with --info to see if artifact is being properly uploaded.

# For maven use case #

* Add below snipped to project's pom
```
<project>
...
<distributionManagement> 
   <snapshotRepository>
     <id>internal-snapshot</id>
     <uniqueVersion>false</uniqueVersion>
     <name>Internal Snapshot Repository</name>
     <url>git:snapshots://git@bitbucket.org:Anjan0791/maven-internal.git</url>
   </snapshotRepository>
   <repository>
     <id>internal-release</id>
     <uniqueVersion>true</uniqueVersion>
     <name>Internal Release Repository</name>
     <url>git:releases://git@bitbucket.org:Anjan0791/maven-internal.git</url>
   </repository>
  </distributionManagement>
  
  <pluginRepositories>
    <pluginRepository>
    <id>synergian-repo</id>
    <url>https://raw.github.com/synergian/wagon-git/releases</url>
   </pluginRepository>
  </pluginRepositories>

  <build>
    <extensions>
     <extension>
      <groupId>ar.com.synergian</groupId>
      <artifactId>wagon-git</artifactId>
      <version>0.2.5</version>
     </extension>
    </extensions>
  </build>
...
</project>
```
* configure settings.xml for authentication

```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
		  https://maven.apache.org/xsd/settings-1.0.0.xsd">
    <servers>
	<server>
	  <id>internal-snapshot</id>
	  <username>email_id</username>
	  <password>passwd</password>
	  <privateKey>${user.home}/.ssh/id_dsa</privateKey>
	  <filePermissions>664</filePermissions>
	  <directoryPermissions>775</directoryPermissions>
	  <configuration></configuration>
	</server>
	<server>
	  <id>internal-release</id>
	  <username>email_id</username>
	  <password>passwd</password>
	  <privateKey>${user.home}/.ssh/id_dsa</privateKey>
	  <filePermissions>664</filePermissions>
	  <directoryPermissions>775</directoryPermissions>
	  <configuration></configuration>
	</server>

    </servers>
</settings>


```